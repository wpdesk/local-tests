PhpStorm tests.


Usage
=====

1. Copy docker-compose.yaml file to **tests** folder in project.

2. Add new Docker Compose CLI Interpreter to project:
![CLI Interpreter](images/cli-interpreter.png)

3. Create new PHPUnit run configuration for integration tests:
![Integration](images/configuration-integration.png)

4. Create new PHPUnit run configuration for unit tests:
![Unit](images/configuration-unit.png)

5. Run tests :)
![RUN](images/run.png)

Docker compose services
-----------------------
File **docker-compose.yaml** contains latest version of PHP and WooCommerce. Latest versions PHP and WooCommerce is in service **wordpress**.
PHP version in numbered in first digit and WooCommerce is numbered in second digit.  
We recommended use **wordpress** service.